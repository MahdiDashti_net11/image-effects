const inputs = document.querySelectorAll('.controls input')

function update() {
    const suffix = this.dataset.size || '';
    document.documentElement.style.setProperty(`--${this.name}`, this.value + suffix)
    console.log(suffix)
}

inputs.forEach(input => input.addEventListener('change', update))